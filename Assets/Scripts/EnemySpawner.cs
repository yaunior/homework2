using System.Collections;
using UnityEngine;
using Random = System.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Transform[] _spawns;
    [SerializeField] private Enemy _prefab;
    [SerializeField] private float _delay;
    
    private Random random = new Random();

    private void Start()
    {
        StartCoroutine(WaitingSpawn(_delay));
    }
    
    private IEnumerator WaitingSpawn(float delay)
    {
        var wait = new WaitForSeconds(delay);

        while (true)
        {
            SpawnEnemy();
            yield return wait;
        }
    }
    
    private void SpawnEnemy()
    {
        int spawnNumber = random.Next(0, _spawns.Length);
        Transform spawn = _spawns[spawnNumber];

        _prefab.transform.position = spawn.transform.position;
        _prefab.GetComponent<EnemyMovment>().Rotation = spawn.transform.rotation;
        Instantiate(_prefab);
    }
}
