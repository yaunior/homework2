using System;
using UnityEngine;

public class EnemyMovment : MonoBehaviour
{
    [SerializeField] private float _speed;

    public Quaternion Rotation;

    private void Start()
    {
        transform.rotation = Rotation;
    }

    private void Update()
    {
        transform.Translate(Vector3.forward * _speed * Time.deltaTime);
    }
}
